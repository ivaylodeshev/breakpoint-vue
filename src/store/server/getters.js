export default {
  isLoggedIn (state) {
    return state.token !== null
  },
  getUrl (state) {
    return state.apiUrl
  }
}
