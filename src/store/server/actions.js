import axios from 'axios'

axios.defaults.baseURL = process.env.API

export default {
  retrieveToken (context, credentials) {
    return new Promise((resolve, reject) => {
      axios.post('/auth', {
        username: credentials.username,
        password: credentials.password
      })
        .then(response => {
          const token = response.data.jwt

          localStorage.setItem('access_token', token)
          context.commit('retrieveToken', token)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  register (context, data) {
    return new Promise((resolve, reject) => {
      axios.post('/register', {
        username: data.username,
        password: data.password,
        code: data.code
      }).then(response => {
        resolve(response)
      })
        .catch(error => {
          reject(error)
        })
    })
  },
  destroyToken (context) {
    localStorage.removeItem('access_token')
    context.commit('destroyToken')
    this.$router.push('/')
  },
  uploadImage (context, data) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
    axios.post('/images/upload', data)
      .catch((err) => {
        console.log(err)
      })
  },
  deleteImage (context, id) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
    return new Promise((resolve, reject) => {
      axios.delete('/images/delete/' + id).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  getAllImages (context, section) {
    return axios.get('/images/' + section).then((res) => res.data)
  },
  getUrl (context, id) {
    return context.state.apiUrl + '/images/getImage/' + id
  },
  // BLOGS
  createBlog (context, data) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
    return axios.post('/blogs/add', data).then((result) => result.data)
  },
  getAllBlogs () {
    return axios.get('/blogs/all').then((res) => res.data)
  },
  deleteBlog (context, id) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
    return axios.delete('/blogs/delete/' + id)
  }
}
