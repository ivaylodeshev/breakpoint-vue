export default function () {
  return {
    token: localStorage.getItem('access_token') || null,
    test: false,
    apiUrl: process.env.API
  }
}
