import Vue from 'vue'
import Vuex from 'vuex'

// we first import the module
import server from './server'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    // then we reference it
    server
  },

  // enable strict mode (adds overhead!)
  // for dev mode only
  strict: process.env.DEV
})

export default store
