
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Index.vue') },
      { path: '/nieuws', component: () => import('pages/Nieuws.vue') },
      { path: '/about', component: () => import('pages/About.vue') },
      { path: '/images', component: () => import('pages/Images.vue'), props: true },
      { path: '/about', component: () => import('pages/About.vue'), props: true },
      { path: '/login', component: () => import('components/auth/Login.vue') },
      { path: '/register', component: () => import('components/auth/Register.vue') },
      { path: '/logout', component: () => import('components/auth/Logout.vue') },
      { path: '/menu', component: () => import('pages/Menu.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
