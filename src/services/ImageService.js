import axios from 'axios'
const config = {
  headers: {
    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJpdm8iLCJpYXQiOjE1ODY0NTA0NTksImV4cCI6MTU4NjQ1MjI1OX0.V5pbdQh97tWY5Rcd4XK8aNHYSclq073Qe19OjyEu53M' }
}
const URL = process.env.API + '/images'

export default class ImageService {
  uploadImage (formData) {
    return axios.post(URL + '/upload', formData, config).then((res) => {
      console.log(res)
    }).catch((err) => {
      console.log(err)
    })
  }
  getAllImages (section) {
    return axios.get(URL + '/' + section).then((res) => res.data)
  }

  getUrl (id) {
    return URL + '/getImage/' + id
  }
}
