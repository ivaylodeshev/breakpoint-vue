import axios from 'axios'

const URL = process.env.API

export default class BlogService {
  createBlog (data) {
    return axios.post(URL + '/add', data).then((result) => result.data)
  }
  getAllBlogs () {
    return axios.get(URL + '/all').then((res) => res.data)
  }
  deleteBlog (id) {
    return axios.delete(URL + '/delete/' + id)
  }
}
